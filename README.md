# Bitbucket Repository Name Plugin #

This plugin helps to identify repository directories on the Bitbucket server.  The plugin populates the `config` file with the project and repository name.

### License ###

Apache License 2.0