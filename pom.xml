<?xml version="1.0" encoding="UTF-8"?>

<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">

    <modelVersion>4.0.0</modelVersion>
    <artifactId>bitbucket-reponame-plugin</artifactId>
    <version>2.0.3-SNAPSHOT</version>

    <parent>
        <artifactId>bitbucket-parent</artifactId>
        <groupId>com.atlassian.bitbucket.server</groupId>
        <!-- When updating the Bitbucket version, also update properties/bitbucket.version -->
        <version>4.0.0</version>
    </parent>

    <properties>
        <!-- When updating the Bitbucket version, also update parent/version -->
        <bitbucket.version>4.0.0</bitbucket.version>
        <amps.version>6.1.0-e937d1f</amps.version>
    </properties>

    <scm>
        <connection>scm:git:ssh://git@bitbucket.org/atlassian/stash-reponame-plugin.git</connection>
        <developerConnection>scm:git:ssh://git@bitbucket.org/atlassian/stash-reponame-plugin.git</developerConnection>
        <url>https://bitbucket.org/atlassian/stash-reponame-plugin</url>
      <tag>HEAD</tag>
  </scm>

    <licenses>
        <license>
            <name>Apache License 2.0</name>
            <url>http://www.apache.org/licenses/LICENSE-2.0</url>
            <distribution>repo</distribution>
        </license>
    </licenses>

    <organization>
        <name>Atlassian</name>
        <url>http://www.atlassian.com/</url>
    </organization>

    <name>Bitbucket Repository Name Plugin</name>
    <description>Populates a repository's description file with the project and repo name.
        This helps to identify repositories on the Bitbucket server.
    </description>
    <packaging>atlassian-plugin</packaging>

    <dependencies>
        <dependency>
            <groupId>com.atlassian.bitbucket.server</groupId>
            <artifactId>bitbucket-api</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.bitbucket.server</groupId>
            <artifactId>bitbucket-spi</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.bitbucket.server</groupId>
            <artifactId>bitbucket-git-api</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.bitbucket.server</groupId>
            <artifactId>bitbucket-util</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.sal</groupId>
            <artifactId>sal-api</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-beans</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-core</artifactId>
            <scope>provided</scope>
        </dependency>

        <!-- Test dependencies -->
        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>com.jayway.restassured</groupId>
            <artifactId>rest-assured</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.plugins.rest</groupId>
            <artifactId>atlassian-rest-common</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.bitbucket.server</groupId>
            <artifactId>bitbucket-test-util</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.bitbucket.server</groupId>
            <artifactId>bitbucket-it-common</artifactId>
            <scope>test</scope>
        </dependency>
    </dependencies>

    <build>
        <resources>
            <resource>
                <directory>src/main/resources</directory>
                <filtering>true</filtering>
                <includes>
                    <include>*.xml</include>
                    <include>**/*.xml</include>
                </includes>
            </resource>
            <resource>
                <directory>src/main/resources</directory>
                <filtering>false</filtering>
                <excludes>
                    <exclude>*.xml</exclude>
                    <exclude>**/*.xml</exclude>
                </excludes>
            </resource>
        </resources>
        <plugins>
            <plugin>
                <groupId>com.atlassian.maven.plugins</groupId>
                <artifactId>bitbucket-maven-plugin</artifactId>
                <extensions>true</extensions>
                <version>${amps.version}</version>
                <configuration>
                    <products>
                        <product>
                            <id>bitbucket</id>
                            <instanceId>bitbucket</instanceId>
                            <version>${bitbucket.version}</version>
                            <dataVersion>${bitbucket.version}</dataVersion>
                        </product>
                    </products>
                    <systemPropertyVariables>
                        <!-- Our func tests don't know how to wait for the system to be available, so force
                             it to come up synchronously so that Cargo will block for us -->
                        <johnson.spring.lifecycle.synchronousStartup>true</johnson.spring.lifecycle.synchronousStartup>
                    </systemPropertyVariables>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-surefire-plugin</artifactId>
                <configuration>
                    <excludes>
                        <exclude>it/**/*.java</exclude>
                    </excludes>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-resources-plugin</artifactId>
                <executions>
                    <execution>
                        <id>copy-license</id>
                        <phase>process-sources</phase>
                        <goals>
                            <goal>copy-resources</goal>
                        </goals>
                        <configuration>
                            <outputDirectory>${project.build.outputDirectory}/META-INF</outputDirectory>
                            <overwrite>true</overwrite>
                            <resources>
                                <resource>
                                    <directory>${user.dir}</directory>
                                    <includes>
                                        <include>LICENSE.txt</include>
                                    </includes>
                                </resource>
                            </resources>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
        </plugins>
    </build>

</project>
